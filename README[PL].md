#### **Instalacja**:

```
git clone https://gitlab.com/ProjektRoot/oblogout-icons
cd oblogout-icons/
cp -r Selena-Orange /usr/share/themes/

edytuj 'oblogout.conf'

nano /etc/oblogout.conf

i dodaj selena-orange do 'buttontheme'

...
buttontheme = selena-orange
... 
```
![screen-shot](/uploads/068fa4f2d2075976cb676abdbae18ce8/screen-shot.png)